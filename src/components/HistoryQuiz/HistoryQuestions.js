import React from 'react';
import CorrectAnswerPng from '../../assets/poprawna_odpowiedź_.png';
export let correctAnswer = <img src={CorrectAnswerPng} alt='correct-answer' />;
export const questions = [
    {
        id: 1,
        questionText: 'Wojna zimowa, a potem tzw. wojna kontynuacyjna, to nazwy starcia:',
        answerOptions: [
            { id: 1, answerText: 'A.Niemiec i Norwegii', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.ZSRR i państw bałtyckich', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.ZSRR i Finlandii', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Blokiem wschodnim a zachodnim', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id: 2,
        questionText: 'Czego dotyczyła operacja "Dynamo"?',
        answerOptions: [
            { id: 1, answerText: 'A.Ewakuacji aliantów z Dunkierki', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Zniszczenia beligijskich twierdz', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Ominięci Linii Maginota', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Zalania Holenderskich dróg', isCorrect: false, result: [] },

        ],
    },
    {
        id: 3,
        questionText: 'Człowiek myślący co',
        answerOptions: [
            { id: 1, answerText: 'A.hemo arabin', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.homo erectus.', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.homo habilis', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.homo sapiens', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id: 4,
        questionText: 'Która z poniższych rzeczy została wynaleziona w II wieku naszej ery?',
        answerOptions: [
            { id: 1, answerText: 'A.Papier', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Proch', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Pieniądz', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Szkło', isCorrect: false, result: [] },
        ],
    },
    {
        id: 5,
        questionText: 'Wskaż siągnięcia homo erectusa - człowieka wyprostowanego',
        answerOptions: [
            { id: 1, answerText: 'A.Budowa kamiennych domów', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Wynalezienie łuku', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Posługiwanie się ogniem', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'C.Wszystkie wyżej wymienione', isCorrect: false, result: [] },
        ],
    },
    {
        id: 6,
        questionText: '622 rok to ważna data w dziejach świata. Co się wtedy wydarzyło?',
        answerOptions: [
            { id: 1, answerText: 'A.Wyparto muzłumanów z Europy', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Doszło do wielkiej schizmy wschodniej', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Mahomet uciekł z Mekki', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Doszło do upadku Cesarstwa Bizantyjskiego', isCorrect: false, result: [] },
        ],
    },
    {
        id: 7,
        questionText: 'Chrzest Polski odbył się 14 kwietnia...',
        answerOptions: [
            { id: 1, answerText: 'A.699r.', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.966r.', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.669r.', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.996r.', isCorrect: false, result: [] },
        ],
    },
    {
        id: 8,
        questionText: 'Czy Władysław Jagiełło zginął w bitwie pod Grunwaldem??',
        answerOptions: [
            { id: 1, answerText: 'A.Tak', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Nie', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id: 9,
        questionText: 'W XVI w. Złota Orda:',
        answerOptions: [
            { id: 1, answerText: 'A.Ostatecznie Upadła', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Powstała', isCorrect: false, result: [] },
        ],
    },
    {
        id: 10,
        questionText: 'W 1789r.:',
        answerOptions: [
            { id: 1, answerText: 'A.Wybuchła insurekcja kościuszkowska', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Napoleon Bonaparte dokonuje zamachu stanu', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Wybuchła rewolucja francuska', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Sejm Wielki uchwalił Konstytucję 3 maja', isCorrect: true, result: (correctAnswer) },
        ],
    },
]

export default { questions, correctAnswer };