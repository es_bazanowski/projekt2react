import React from 'react';
import CorrectAnswerPng from '../../assets/poprawna_odpowiedź_.png';
export let correctAnswer = <img src={CorrectAnswerPng} alt='correct-answer' />;
export const questions = [
    {
        id:1,
        questionText: 'W którym roku firma apple wprowadziła do sprzedaży pierwszy komputer?',
        answerOptions: [
            { id: 1, answerText: 'A.1974', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.1976', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.1984', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.1990', isCorrect: false, result: [] },
        ],
    },
    {
        id:2,
        questionText: 'Jak nazywa się pierwsza komercyjna przeglądarka internetowa przeznaczona do użytku domowego?',
        answerOptions: [
            { id: 1, answerText: 'A.Internet Explorer', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Netscape Navigator', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Mosaic', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Firefox', isCorrect: false, result: [] },

        ],
    },
    {
        id:3,
        questionText: 'Ktora z firm nie jest producentem akcesoriów komputerowych?',
        answerOptions: [
            { id: 1, answerText: 'A.SAP', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Razer', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Steelseries', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Logitech', isCorrect: false, result: [] },
        ],
    },
    {
        id:4,
        questionText: 'Co oznacza skrót BIOS?',
        answerOptions: [
            { id: 1, answerText: 'A.Basic Input Output System', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Byte Integration Operations System', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Basic Interconnect Operating System', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Basic Iteration Operate System', isCorrect: false, result: [] },
        ],
    },
    {
        id:5,
        questionText: 'Która z nazw nie oznacza systemu operacyjnego?',
        answerOptions: [
            { id: 1, answerText: 'A.Windows', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Linux', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Chrome', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Ubuntu', isCorrect: false, result: [] },
        ],
    },
    {
        id:6,
        questionText: 'W którym roku powstał facebook?',
        answerOptions: [
            { id: 1, answerText: 'A.2005', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.2004', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.2002', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.2006', isCorrect: false, result: [] },
        ],
    },
    {
        id:7,
        questionText: 'W którym roku została uruchomiona poczta Gmail?',
        answerOptions: [
            { id: 1, answerText: 'A.2003', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.2004', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.2006', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.2008', isCorrect: false, result: [] },
        ],
    },
    {
        id:8,
        questionText: 'Co oznacza skrót SSD?',
        answerOptions: [
            { id: 1, answerText: 'A.Super Solid Drive', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Solid Super Drive', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Super Speed Drive', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Solid State Drive', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:9,
        questionText: 'Co oznacza skrót CPU?',
        answerOptions: [
            { id: 1, answerText: 'A.Central Processing Unit', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Critical Processing Unit', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Crucial Processing Unit', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Center Proces Unity', isCorrect: false, result: [] },
        ],
    },
    {
        id:10,
        questionText: 'Samsung to firma z:',
        answerOptions: [
            { id: 1, answerText: 'A.Korei', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Chin', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Japonii', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Wietnamu', isCorrect: false, result: [] },
        ],
    },
]

export default { questions, correctAnswer };