import React from 'react';
import CorrectAnswerPng from '../../assets/poprawna_odpowiedź_.png';
export let correctAnswer = <img src={CorrectAnswerPng} alt='correct-answer' />;
export const questions = [
    {
        id:1,
        questionText: 'Czym jest inkrementacja?',
        answerOptions: [
            { id: 1, answerText: 'A.Zmniejszenie wartości o 1', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Zwiększenie wartości o n, gdzie n>=2', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Zwiększenie wartości o 1', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Zmiana wartości na 0', isCorrect: false, result: [] },
        ],
    },
    {
        id:2,
        questionText: 'Czym jest tzw. IDE:',
        answerOptions: [
            { id: 1, answerText: 'A.Algorytm', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Środowisko programistyczne', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Biblioteka przydatnych funkcji', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Potoczna nazwa Terminalu', isCorrect: false, result: [] },

        ],
    },
    {
        id:3,
        questionText: 'Od jakiej liczby zaczyna się indeksacja tablic?',
        answerOptions: [
            { id: 1, answerText: 'A.-1', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.1.', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.2', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.0', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:4,
        questionText: 'Przy pomocy klauzuli ORDER BY języka SQL wykonamy:',
        answerOptions: [
            { id: 1, answerText: 'A.Grupowanie', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Usuwanie', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Sortowanie', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Zawężenie wyników wyszukiwania', isCorrect: false, result: [] },
        ],
    },
    {
        id:5,
        questionText: 'Charakterystycznym językiem programowania back-end’owym jest:',
        answerOptions: [
            { id: 1, answerText: 'A.PHP', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.JavaScript', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.CSS', isCorrect: false, result: [] },
        ],
    },
    {
        id:6,
        questionText: 'W momencie gdy musimy sprawdzić czy wartość jest prawdą użyjemy:',
        answerOptions: [
            { id: 1, answerText: 'A.if', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.while', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.for', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.switch', isCorrect: false, result: [] },
        ],
    },
    {
        id:7,
        questionText: 'Jak dodać komentarz jedno linijkowy w JS:',
        answerOptions: [
            { id: 1, answerText: 'A./*', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.$', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.%%', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.//', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:8,
        questionText: 'Jeśli pracujesz w hiszpańskiej firmie to w jakim języku powinieneś pisać komentarze?',
        answerOptions: [
            { id: 1, answerText: 'A.Polskim', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Angielskim', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Hiszpańskim', isCorrect: false, result: [] },
        ],
    },
    {
        id:9,
        questionText: 'Wartość TRUE i FALSE to:',
        answerOptions: [
            { id: 1, answerText: 'A.String', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Boolean', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Int', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Class', isCorrect: false, result: [] },
        ],
    },
    {
        id:10,
        questionText: 'Jeśli chcemy zwrócić resztę z dzielenia użyjemy:',
        answerOptions: [
            { id: 1, answerText: 'A./', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.**', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.%', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.+', isCorrect: false, result: [] },
        ],
    },
]

export default { questions, correctAnswer };