import technology_icon from '../../assets/technologia_ikona_.png';
import culture_icon from '../../assets/kultura_ikona.png'
import automotive_icon from '../../assets/motoryzacja_ikona.png'
import programming_icon from '../../assets/programowanie_ikona.png'
import history_icon from '../../assets/historia_ikona.png'
import { Link } from 'react-router-dom';

const QuizList = () => {
    return (
        <div className='main-quizList-wrapper'>
            <Link to='/technology'><div className='quiz-button_wrapper'>
                <div><img src={technology_icon} alt='technology_icon' /></div>
                <div className='quiz-button_wrapper_line'></div>
                <div>TECHNOLOGIA</div>
            </div>
            </Link>
            <Link to='/culture'><div className='quiz-button_wrapper'>
                <div><img src={culture_icon} alt='culture_icon' /></div>
                <div className='quiz-button_wrapper_line'></div>
                <div>KULTURA</div>
            </div>
            </Link>
            <Link to='/automotive'><div className='quiz-button_wrapper'>
                <div><img src={automotive_icon} alt='automotive_icon' /></div>
                <div className='quiz-button_wrapper_line'></div>
                <div>MOTORYZACJA</div>
            </div>
            </Link>
            <Link to='/programming'><div className='quiz-button_wrapper'>
                <div><img src={programming_icon} alt='programming' /></div>
                <div className='quiz-button_wrapper_line'></div>
                <div>PROGRAMOWANIE</div>
            </div>
            </Link>
            <Link to='/history'><div className='quiz-button_wrapper'>
                <div><img src={history_icon} alt='quiz_icon' /></div>
                <div className='quiz-button_wrapper_line'></div>
                <div>HISTORIA</div>
            </div>
            </Link>

        </div>
    )
}

export default QuizList;