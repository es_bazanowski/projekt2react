import QuizList from '../../components/quizList';

const Homepage = () => {

    return (
        <div className='main-homepage-wrapper'>
            <div className='main-homepage-wrapper__icon'><span>Q</span></div>
            <div className='main-homepage-wrapper__heading'><h1>QUIZ</h1></div>
            <div className='main-homepage-wrapper__about'><p>10 PYTAŃ/ 5 KATEGORII</p></div>
            <div className='main-homepage-wrapper__categories'><QuizList /></div>

        </div>
    )
}

export default Homepage;