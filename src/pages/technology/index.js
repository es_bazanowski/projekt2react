import technology_icon from '../../assets/technologia_ikona_.png';
import BackButton from '../../assets/cofnij_x.png'
import QuitButton from '../../assets/zamknij_x.png'
import { Link } from 'react-router-dom';
const Technology = () => {
    return (
        <div className='selected-category-wrapper main-technology-background'>
            <div className='selected-category-wrapper__icon'><span>Q</span></div>
            <div className='selected-category-wrapper-header'><div><h1>QUIZ</h1></div>
                <div className='header_btn'><Link to='/'><img src={BackButton} alt='back_button' /></Link></div>
                <div className='header_btn'><Link to='/'><img src={QuitButton} alt='quit_button' /></Link></div>
            </div>
            <div className='selected-category-wrapper__selCategory pickCat_technology_background'><p>WYBRANA KATEGORIA:</p></div>
            <div className='selected-category-wrapper__category'><div className='main-category_wrapper'>
                <div className='category__icon'><img src={technology_icon} alt='technology_icon' /></div>
                <div className='category__line technology-summary__decorationLine'></div>
                <div className='category__text'>TECHNOLOGIA</div>
                <Link to='/techQuiz'><div className='category__btn technology-start-button-background'>
                    <div><p>ROZPOCZNIJ</p></div>
                    <div className='category__start technology-category__start'></div>
                </div></Link>
            </div></div>

        </div>
    )
}

export default Technology;